<?php

/**
 * @file
 * Contains weekly_report.page.inc.
 *
 * Page callback for Weekly report entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Weekly report templates.
 *
 * Default template: weekly_report.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_weekly_report(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
