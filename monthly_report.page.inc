<?php

/**
 * @file
 * Contains monthly_report.page.inc.
 *
 * Page callback for Monthly report entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Monthly report templates.
 *
 * Default template: monthly_report.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_monthly_report(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
