CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Clockify Report provides an option to generate the weekly and
monthly reports based on the user and project and csv export option.

REQUIREMENTS
------------

This module requires the following outside of Drupal core.

 * Clockify - https://www.drupal.org/project/clockify

INSTALLATION
------------

 * Install the Clockify Report module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

CONFIGURATION
-------------

No configuration is needed.

MAINTAINERS
-----------

 * anmol goel (anmolgoyal74) - https://www.drupal.org/u/anmolgoyal74
 * meenakshi gupta (Meenakshi.g) - https://www.drupal.org/u/meenakshig

For a full list of contributors visit:

 * https://www.drupal.org/node/3102936/committers
