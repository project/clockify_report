<?php

namespace Drupal\clockify_report\Controller;

use Dompdf\Dompdf;
use Drupal\clockify\Entity\clockify;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Views;

/**
 * Class ClockifyReportInvoiceController generates an invoice pdf file.
 */
class ClockifyReportInvoiceController extends ControllerBase {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ClockifyReportInvoiceController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Generate Invoice PDF.
   *
   * @return array
   *   Render array.
   */
  public function generateInvoicePdf() {
    $values = [];
    $subtotal = 0;
    $headers = [
      $this->t('Project'),
      $this->t('Consultant Name'),
      $this->t('Remote Unit/Hours'),
      $this->t('Onsite Unit/Hours'),
      $this->t('Remote Price'),
      $this->t('Onsite Price'),
      $this->t('Total'),
    ];
    $headers_vat = [
      $this->t('VAT %'),
      $this->t('Amount'),
      $this->t('VAT'),
      $this->t('Total'),
    ];
    $view = Views::getView('monthly_report');
    $view->setDisplay('page_1');
    $view->getExposedInput();
    $view->execute();
    foreach ($view->result as $rid => $row) {
      $project_id = $row->_entity->get('field_project')->getValue()['0']['target_id'];
      $clockify = clockify::load($project_id);
      $values[$rid]['project'] = $clockify->name->value;
      $user_id = $row->_entity->get('field_name')->getValue()['0']['target_id'];
      $user = $this->entityTypeManagerUser->getStoraget('user')->load($user_id);
      $values[$rid]['name'] = $user->getUsername();
      // Remote total duration.
      $remote_hours = number_format($this->decimalHours($row->_entity->get('field_remote_total_duration')->getValue()['0']['value']), 2);
      $values[$rid]['remote_total_duration'] = $remote_hours;
      // Onsite total duration.
      $onsite_hours = number_format($this->decimalHours($row->_entity->get('field_onsite_total_duration')->getValue()['0']['value']), 2);
      $values[$rid]['onsite_total_duration'] = $onsite_hours;
      // Remote price.
      $values[$rid]['remote_price'] = $user->field_remote_price->value;
      // Onsite price.
      $values[$rid]['onsite_price'] = $user->field_onsite_price->value;
      // Total.
      $values[$rid]['total'] = number_format($remote_hours * $user->field_remote_price->value + $onsite_hours * $user->field_onsite_price->value, 2);
      $total = $remote_hours * $user->field_remote_price->value + $onsite_hours * $user->field_onsite_price->value;
      $subtotal += $total;
    }
    $subtotal = number_format($subtotal, 2);
    $footer = [
      [
        'data' => [
      ['data' => 'Total', 'colspan' => 6],
          $subtotal,
        ],
      ],
    ];
    $report = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $values,
      '#footer' => $footer,
    ];

    $vat = [
      '#type' => 'table',
      '#header' => $headers_vat,
      '#rows' => [
      ['0%', $subtotal, '-', $subtotal],
      ],
    ];

    $pay_total = [
      '#type' => 'table',
      '#header' => [$this->t('To Pay')],
      '#rows' => [[$subtotal]],
    ];

    $response = [
      '#theme' => 'invoice_pdf',
      '#table' => $report,
      '#vat' => $vat,
      '#paytotal' => $pay_total,
      '#attached' => [
        'library' => [
          'clockify_report/invoice-pdf',
        ],
      ],
    ];
    $dompdf = new Dompdf(
          [
            'enable_remote' => TRUE,
            'isHtml5ParserEnabled' => TRUE,
          ]
      );
    $dompdf->loadHtml(render($response));
    $dompdf->setPaper('A4', 'portrait');
    $dompdf->render();
    print_r($dompdf->stream('company-monthly-invoice.pdf'));
    return $dompdf->stream('company-monthly-invoice.pdf');
  }

  /**
   * Converts duration value into hours.
   */
  public function decimalHours($duration) {
    if (!empty($duration)) {
      $interval = new \DateInterval($duration);
      $formatted_duration = $interval->format('%H:%I:%S');
      $hms = explode(":", $formatted_duration);
      return ($hms[0] + ($hms[1] / 60) + ($hms[2] / 3600));
    }
    else {
      return;
    }

  }

}
