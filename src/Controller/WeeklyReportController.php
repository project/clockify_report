<?php

namespace Drupal\clockify_report\Controller;

use Drupal\clockify_report\Entity\WeeklyReportInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WeeklyReportController.
 *
 *  Returns responses for Weekly report routes.
 */
class WeeklyReportController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Weekly report revision.
   *
   * @param int $weekly_report_revision
   *   The Weekly report revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($weekly_report_revision) {
    $weekly_report = $this->entityTypeManager()->getStorage('weekly_report')
      ->loadRevision($weekly_report_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('weekly_report');

    return $view_builder->view($weekly_report);
  }

  /**
   * Page title callback for a Weekly report revision.
   *
   * @param int $weekly_report_revision
   *   The Weekly report revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($weekly_report_revision) {
    $weekly_report = $this->entityTypeManager()->getStorage('weekly_report')
      ->loadRevision($weekly_report_revision);
    return $this->t(
          'Revision of %title from %date', [
            '%title' => $weekly_report->label(),
            '%date' => $this->dateFormatter->format($weekly_report->getRevisionCreationTime()),
          ]
      );
  }

  /**
   * Generates an overview table of older revisions of a Weekly report.
   *
   * @param \Drupal\clockify_report\Entity\WeeklyReportInterface $weekly_report
   *   A Weekly report object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(WeeklyReportInterface $weekly_report) {
    $account = $this->currentUser();
    $weekly_report_storage = $this->entityTypeManager()->getStorage('weekly_report');

    $langcode = $weekly_report->language()->getId();
    $langname = $weekly_report->language()->getName();
    $languages = $weekly_report->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ?
                      $this->t('@langname revisions for %title', [
                        '@langname' => $langname,
                        '%title' => $weekly_report->label(),
                      ]) :
                      $this->t('Revisions for %title', [
                        '%title' => $weekly_report->label(),
                      ]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all weekly report revisions") || $account->hasPermission('administer weekly report entities')));
    $delete_permission = (($account->hasPermission("delete all weekly report revisions") || $account->hasPermission('administer weekly report entities')));

    $rows = [];

    $vids = $weekly_report_storage->revisionIds($weekly_report);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /**
       * @var \Drupal\clockify_report\WeeklyReportInterface $revision
       */
      $revision = $weekly_report_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $weekly_report->getRevisionId()) {
          /*
          @todo Drupal Rector Notice:
          Please delete the following comment after you've
          made any necessary changes.
          Please manually remove the `use LinkGeneratorTrait;`
          statement from this class.
           */
          $link = Link::fromTextAndUrl(
                $date, new Url(
                    'entity.weekly_report.revision', [
                      'weekly_report' => $weekly_report->id(),
                      'weekly_report_revision' => $vid,
                    ]
                )
            );
        }
        else {
          /*
          @todo Drupal Rector Notice: Please delete the following comment
          after you've made any necessary changes.
          Please confirm that `$weekly_report` is an
          instance of `\Drupal\Core\Entity\EntityInterface`.
          Only the method name and not the class name
          was checked for this replacement,
          so this may be a false positive.
           */
          $link = $weekly_report->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute(
                  'entity.weekly_report.translation_revert', [
                    'weekly_report' => $weekly_report->id(),
                    'weekly_report_revision' => $vid,
                    'langcode' => $langcode,
                  ]
              ) :
              Url::fromRoute(
                  'entity.weekly_report.revision_revert', [
                    'weekly_report' => $weekly_report->id(),
                    'weekly_report_revision' => $vid,
                  ]
              ),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute(
                  'entity.weekly_report.revision_delete', [
                    'weekly_report' => $weekly_report->id(),
                    'weekly_report_revision' => $vid,
                  ]
              ),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['weekly_report_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
