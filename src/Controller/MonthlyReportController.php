<?php

namespace Drupal\clockify_report\Controller;

use Drupal\clockify_report\Entity\MonthlyReportInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MonthlyReportController.
 *
 *  Returns responses for Monthly report routes.
 */
class MonthlyReportController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Monthly report revision.
   *
   * @param int $monthly_report_revision
   *   The Monthly report revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($monthly_report_revision) {
    $monthly_report = $this->entityTypeManager()->getStorage('monthly_report')
      ->loadRevision($monthly_report_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('monthly_report');

    return $view_builder->view($monthly_report);
  }

  /**
   * Page title callback for a Monthly report revision.
   *
   * @param int $monthly_report_revision
   *   The Monthly report revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($monthly_report_revision) {
    $monthly_report = $this->entityTypeManager()->getStorage('monthly_report')
      ->loadRevision($monthly_report_revision);
    return $this->t(
          'Revision of %title from %date', [
            '%title' => $monthly_report->label(),
            '%date' => $this->dateFormatter->format($monthly_report->getRevisionCreationTime()),
          ]
      );
  }

  /**
   * Generates an overview table of older revisions of a Monthly report.
   *
   * @param \Drupal\clockify_report\Entity\MonthlyReportInterface $monthly_report
   *   A Monthly report object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(MonthlyReportInterface $monthly_report) {
    $account = $this->currentUser();
    $monthly_report_storage = $this->entityTypeManager()->getStorage('monthly_report');

    $langcode = $monthly_report->language()->getId();
    $langname = $monthly_report->language()->getName();
    $languages = $monthly_report->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ?
                       $this->t('@langname revisions for %title', [
                         '@langname' => $langname,
                         '%title' => $monthly_report->label(),
                       ]) :
                       $this->t('Revisions for %title', [
                         '%title' => $monthly_report->label(),
                       ]);
    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all monthly report revisions") || $account->hasPermission('administer monthly report entities')));
    $delete_permission = (($account->hasPermission("delete all monthly report revisions") || $account->hasPermission('administer monthly report entities')));

    $rows = [];

    $vids = $monthly_report_storage->revisionIds($monthly_report);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /**
       * @var \Drupal\clockify_report\MonthlyReportInterface $revision
       */
      $revision = $monthly_report_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $monthly_report->getRevisionId()) {
          /*
          @todo Drupal Rector Notice: Please delete the
          following comment after you've made any necessary changes.
          Please manually remove the `use LinkGeneratorTrait;`
          statement from this class.
           */
          $link = Link::fromTextAndUrl(
                $date, new Url(
                    'entity.monthly_report.revision', [
                      'monthly_report' => $monthly_report->id(),
                      'monthly_report_revision' => $vid,
                    ]
                )
            );
        }
        else {
          /*
          @todo Drupal Rector Notice: Please delete the following
          comment after you've made any necessary changes.
          Please confirm that `$monthly_report`
          is an instance of `\Drupal\Core\Entity\EntityInterface`.
          Only the method name and not the class name
          was checked for this replacement,
          so this may be a false positive.
           */
          $link = $monthly_report->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute(
                  'entity.monthly_report.translation_revert', [
                    'monthly_report' => $monthly_report->id(),
                    'monthly_report_revision' => $vid,
                    'langcode' => $langcode,
                  ]
              ) :
              Url::fromRoute(
                  'entity.monthly_report.revision_revert', [
                    'monthly_report' => $monthly_report->id(),
                    'monthly_report_revision' => $vid,
                  ]
              ),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute(
                  'entity.monthly_report.revision_delete', [
                    'monthly_report' => $monthly_report->id(),
                    'monthly_report_revision' => $vid,
                  ]
              ),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['monthly_report_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
