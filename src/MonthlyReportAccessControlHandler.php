<?php

namespace Drupal\clockify_report;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Monthly report entity.
 *
 * @see \Drupal\clockify_report\Entity\MonthlyReport.
 */
class MonthlyReportAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /**
* @var \Drupal\clockify_report\Entity\MonthlyReportInterface $entity
*/

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished monthly report entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published monthly report entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit monthly report entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete monthly report entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add monthly report entities');
  }

}
