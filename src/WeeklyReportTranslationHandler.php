<?php

namespace Drupal\clockify_report;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for weekly_report.
 */
class WeeklyReportTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
