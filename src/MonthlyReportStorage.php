<?php

namespace Drupal\clockify_report;

use Drupal\clockify_report\Entity\MonthlyReportInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Monthly report entities.
 *
 * This extends the base storage class, adding required special handling for
 * Monthly report entities.
 *
 * @ingroup clockify_report
 */
class MonthlyReportStorage extends SqlContentEntityStorage implements MonthlyReportStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(MonthlyReportInterface $entity) {
    return $this->database->query(
          'SELECT vid FROM {monthly_report_revision} WHERE id=:id ORDER BY vid',
          [':id' => $entity->id()]
      )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
          'SELECT vid FROM {monthly_report_field_revision} WHERE uid = :uid ORDER BY vid',
          [':uid' => $account->id()]
      )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(MonthlyReportInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {monthly_report_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('monthly_report_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
