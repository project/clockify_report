<?php

namespace Drupal\clockify_report\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Weekly report entities.
 *
 * @ingroup clockify_report
 */
interface WeeklyReportInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Weekly report name.
   *
   * @return string
   *   Name of the Weekly report.
   */
  public function getName();

  /**
   * Sets the Weekly report name.
   *
   * @param string $name
   *   The Weekly report name.
   *
   * @return \Drupal\clockify_report\Entity\WeeklyReportInterface
   *   The called Weekly report entity.
   */
  public function setName($name);

  /**
   * Gets the Weekly report creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Weekly report.
   */
  public function getCreatedTime();

  /**
   * Sets the Weekly report creation timestamp.
   *
   * @param int $timestamp
   *   The Weekly report creation timestamp.
   *
   * @return \Drupal\clockify_report\Entity\WeeklyReportInterface
   *   The called Weekly report entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Weekly report revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Weekly report revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\clockify_report\Entity\WeeklyReportInterface
   *   The called Weekly report entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Weekly report revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Weekly report revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\clockify_report\Entity\WeeklyReportInterface
   *   The called Weekly report entity.
   */
  public function setRevisionUserId($uid);

}
