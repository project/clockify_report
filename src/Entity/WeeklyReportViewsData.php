<?php

namespace Drupal\clockify_report\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Weekly report entities.
 */
class WeeklyReportViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
