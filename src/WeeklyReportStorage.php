<?php

namespace Drupal\clockify_report;

use Drupal\clockify_report\Entity\WeeklyReportInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Weekly report entities.
 *
 * This extends the base storage class, adding required special handling for
 * Weekly report entities.
 *
 * @ingroup clockify_report
 */
class WeeklyReportStorage extends SqlContentEntityStorage implements WeeklyReportStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(WeeklyReportInterface $entity) {
    return $this->database->query(
          'SELECT vid FROM {weekly_report_revision} WHERE id=:id ORDER BY vid',
          [':id' => $entity->id()]
      )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
          'SELECT vid FROM {weekly_report_field_revision} WHERE uid = :uid ORDER BY vid',
          [':uid' => $account->id()]
      )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(WeeklyReportInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {weekly_report_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('weekly_report_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
