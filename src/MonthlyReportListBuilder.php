<?php

namespace Drupal\clockify_report;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Monthly report entities.
 *
 * @ingroup clockify_report
 */
class MonthlyReportListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Monthly report ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
* @var \Drupal\clockify_report\Entity\MonthlyReport $entity
*/
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
          $entity->label(),
          'entity.monthly_report.edit_form',
          ['monthly_report' => $entity->id()]
      );
    return $row + parent::buildRow($entity);
  }

}
