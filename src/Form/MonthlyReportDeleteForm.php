<?php

namespace Drupal\clockify_report\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Monthly report entities.
 *
 * @ingroup clockify_report
 */
class MonthlyReportDeleteForm extends ContentEntityDeleteForm {


}
