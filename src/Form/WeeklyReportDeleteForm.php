<?php

namespace Drupal\clockify_report\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Weekly report entities.
 *
 * @ingroup clockify_report
 */
class WeeklyReportDeleteForm extends ContentEntityDeleteForm {


}
