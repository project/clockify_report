<?php

namespace Drupal\clockify_report\Form;

use Drupal\clockify_report\Entity\WeeklyReportInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Weekly report revision.
 *
 * @ingroup clockify_report
 */
class WeeklyReportRevisionRevertForm extends ConfirmFormBase {

  /**
   * The Weekly report revision.
   *
   * @var \Drupal\clockify_report\Entity\WeeklyReportInterface
   */
  protected $revision;

  /**
   * The Weekly report storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $weeklyReportStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->weeklyReportStorage = $container->get('entity_type.manager')->getStorage('weekly_report');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weekly_report_revision_revert_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
          'Are you sure you want to revert to the revision from %revision-date?', [
            '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
          ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.weekly_report.version_history', ['weekly_report' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $weekly_report_revision = NULL) {
    $this->revision = $this->WeeklyReportStorage->loadRevision($weekly_report_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The revision timestamp will be updated when the revision is saved. Keep
    // the original one for the confirmation message.
    $original_revision_timestamp = $this->revision->getRevisionCreationTime();

    $this->revision = $this->prepareRevertedRevision($this->revision, $form_state);
    $this->revision->revision_log = $this->t(
          'Copy of the revision from %date.', [
            '%date' => $this->dateFormatter->format($original_revision_timestamp),
          ]
      );
    $this->revision->save();

    $this->logger('content')->notice(
          'Weekly report: reverted %title revision %revision.', [
            '%title' => $this->revision->label(),
            '%revision' => $this->revision->getRevisionId(),
          ]
      );
    $this->messenger()->addMessage(
          $this->t(
              'Weekly report %title has been reverted to the revision from %revision-date.', [
                '%title' => $this->revision->label(),
                '%revision-date' => $this->dateFormatter->format($original_revision_timestamp),
              ]
          )
      );
    $form_state->setRedirect(
          'entity.weekly_report.version_history',
          ['weekly_report' => $this->revision->id()]
      );
  }

  /**
   * Prepares a revision to be reverted.
   *
   * @param \Drupal\clockify_report\Entity\WeeklyReportInterface $revision
   *   The revision to be reverted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\clockify_report\Entity\WeeklyReportInterface
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(WeeklyReportInterface $revision, FormStateInterface $form_state) {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);
    $revision->setRevisionCreationTime(\Drupal::time()->getRequestTime());

    return $revision;
  }

}
