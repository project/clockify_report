<?php

namespace Drupal\clockify_report;

use Drupal\clockify_report\Entity\MonthlyReportInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Monthly report entities.
 *
 * This extends the base storage class, adding required special handling for
 * Monthly report entities.
 *
 * @ingroup clockify_report
 */
interface MonthlyReportStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Monthly report revision IDs for a specific Monthly report.
   *
   * @param \Drupal\clockify_report\Entity\MonthlyReportInterface $entity
   *   The Monthly report entity.
   *
   * @return int[]
   *   Monthly report revision IDs (in ascending order).
   */
  public function revisionIds(MonthlyReportInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Monthly report author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Monthly report revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\clockify_report\Entity\MonthlyReportInterface $entity
   *   The Monthly report entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(MonthlyReportInterface $entity);

  /**
   * Unsets the language for all Monthly report with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
