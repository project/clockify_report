<?php

namespace Drupal\clockify_report;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Weekly report entities.
 *
 * @ingroup clockify_report
 */
class WeeklyReportListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Weekly report ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
* @var \Drupal\clockify_report\Entity\WeeklyReport $entity
*/
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
          $entity->label(),
          'entity.weekly_report.edit_form',
          ['weekly_report' => $entity->id()]
      );
    return $row + parent::buildRow($entity);
  }

}
