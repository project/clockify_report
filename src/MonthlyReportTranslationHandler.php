<?php

namespace Drupal\clockify_report;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for monthly_report.
 */
class MonthlyReportTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
