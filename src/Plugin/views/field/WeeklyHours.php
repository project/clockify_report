<?php

namespace Drupal\clockify_report\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form element for displaying total hours with color indication.
 *
 * @ViewsField("weekly_hours")
 */
class WeeklyHours extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a new WeeklyHours object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('entity_type.manager'),
          $container->get('request_stack')->getCurrentRequest()
      );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $threshold_hours = $this->request->query->get('hours');
    if (!$threshold_hours || !is_numeric($threshold_hours)) {
      $threshold_hours = '40';
    }
    $threshold_value = $threshold_hours * 3600;
    $weekly_report = $values->_entity;
    $total_duration = $weekly_report->field_total_duration->value;
    $current_time = new \DateTime();
    $dt = new \DateTime();
    if ($total_duration !== NULL) {
      $dt->add(new \DateInterval($total_duration));
      $interval = $dt->diff(new \DateTime());
      $formatted_duration = ($interval->d * 24) + $interval->h . 'h ' . $interval->i . 'm ' . $interval->s . 's';
    }
    else {
      $formatted_duration = 0;
    }
    // $formatted_duration = $interval->format('%Hh %Im %Ss');
    $interval_in_sec = abs($dt->getTimestamp() - $current_time->getTimestamp());
    $output['#attached']['library'][] = 'clockify_report/weeekly-report-status';
    if ($weekly_report->status->value == '1') {
      $class = 'green';
    }
    elseif ($weekly_report->field_split_week->value == '1') {
      $class = 'orange';
    }
    elseif ($interval_in_sec == $threshold_value) {
      $class = 'green';
    }
    else {
      $class = 'red';
    }
    $output['#prefix'] = '<span class="' . $class . '">';
    $output['#suffix'] = '</span>';
    $output['duration'] = [
      '#type' => 'markup',
      '#markup' => $formatted_duration,
    ];
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
