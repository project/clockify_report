<?php

namespace Drupal\clockify_report\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views\Views;

/**
 * Defines a views area plugin.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("download_invoice")
 */
class DownloadInvoice extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $month = \Drupal::request()->get('month');
    $year = \Drupal::request()->get('year');
    $view = Views::getView('monthly_report');
    $view->setDisplay('page_1');
    $view->getExposedInput();
    $view->execute();
    $input = $view->getExposedInput();
    $month = $input['month'];
    $year = $input['year'];
    $project = $input['project'];
    $client = $input['client'];
    $onwer = $input['owner'];
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $url = $host . '/clockify_report/generate_invoice?year=' . $year . '&month=' . $month . '&project=' . $project . '&client=' . $client . '&owner=' . $onwer;
    $invoice_button = [
      '#theme' => 'invoice_download',
      '#url' => $url,
      '#theme_wrappers' => [
        'container' => [
          '#attributes' => [
            'class' => [
              'views-invoice-export',
            ],
          ],
        ],
      ],
    ];
    return $invoice_button;
  }

}
