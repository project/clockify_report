<?php

namespace Drupal\clockify_report;

use Drupal\clockify_report\Entity\WeeklyReportInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Weekly report entities.
 *
 * This extends the base storage class, adding required special handling for
 * Weekly report entities.
 *
 * @ingroup clockify_report
 */
interface WeeklyReportStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Weekly report revision IDs for a specific Weekly report.
   *
   * @param \Drupal\clockify_report\Entity\WeeklyReportInterface $entity
   *   The Weekly report entity.
   *
   * @return int[]
   *   Weekly report revision IDs (in ascending order).
   */
  public function revisionIds(WeeklyReportInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Weekly report author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Weekly report revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\clockify_report\Entity\WeeklyReportInterface $entity
   *   The Weekly report entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(WeeklyReportInterface $entity);

  /**
   * Unsets the language for all Weekly report with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
