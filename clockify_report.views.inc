<?php

/**
 * @file
 * clockify_report.views.inc
 */

/**
 * Implements hook_views_data().
 */
function clockify_report_views_data() {
  $data['views']['download_invoice'] = [
    'title' => t('Download Invoice Button'),
    'help' => t('Place this button to download the invoice.'),
    'area' => [
      'id' => 'download_invoice',
    ],
  ];
  return $data;
}
